Name: CLEOII_2001_I537154
Year: 2001
Summary: Kinematic distributions in the decay $D^0\to K^-\pi^+\pi^0$
Experiment: CLEOII
Collider: CESR
InspireID: 537154
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys. Rev. D78:052001, 2008
RunInfo: Any process producing D0
Description:
  'Kinematic distributions  in the decay $D^0\to K^-\pi^+\pi^0$'
ValidationInfo:
  'Compared Herwig events using the same model for the decay as in the paper. The data were read from the plots in the paper.
   Resolution/acceptance effects have been not unfolded, but the efficiency function given in the paer has been applied. Given the agreement with the model in the paper this analysis should only be used for qualitative studies.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Kopp:2000gv
BibTeX: '@article{Kopp:2000gv,
      author         = "Kopp, S. and others",
      title          = "{Dalitz analysis of the decay D0 ---&gt; K- pi+ pi0}",
      collaboration  = "CLEO",
      journal        = "Phys. Rev.",
      volume         = "D63",
      year           = "2001",
      pages          = "092001",
      doi            = "10.1103/PhysRevD.63.092001",
      eprint         = "hep-ex/0011065",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "CLNS-00-1700, CLEO-00-23",
      SLACcitation   = "%%CITATION = HEP-EX/0011065;%%"
}'
