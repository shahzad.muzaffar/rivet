BEGIN PLOT /H1_2006_I699835/d01-x01-y01
Title=$\tau_c$ distribution for $<Q> = 15$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
LogY=10
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d02-x01-y01
Title=$\tau_c$ distribution for $<Q> = 18$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d03-x01-y01
Title=$\tau_c$ distribution for $<Q> = 24$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d04-x01-y01
Title=$\tau_c$ distribution for $<Q> = 37$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d05-x01-y01
Title=$\tau_c$ distribution for $<Q> = 58$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d06-x01-y01
Title=$\tau_c$ distribution for $<Q> = 81$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d07-x01-y01
Title=$\tau_c$ distribution for $<Q> = 116$GeV
XLabel=$\tau_c$
YLabel=$1/\sigma$ d$\sigma/$d$\tau_c$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d08-x01-y01
Title=$\tau$ distribution for $<Q> = 15$GeV
XLabel=$\tau$
YLabel=[Insert $y$-axis label for histogram d01-x01-y01 here]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d09-x01-y01
Title=$\tau$ distribution for $<Q> = 18$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d10-x01-y01
Title=$\tau$ distribution for $<Q> = 24$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d11-x01-y01
Title=$\tau$ distribution for $<Q> = 37$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d12-x01-y01
Title=$\tau$ distribution for $<Q> = 58$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d13-x01-y01
Title=$\tau$ distribution for $<Q> = 81$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d14-x01-y01
Title=$\tau$ distribution for $<Q> = 116$GeV
XLabel=$\tau$
YLabel=$1/\sigma$ d$\sigma/$d$\tau$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d15-x01-y01
Title=$B$ distribution for $<Q> = 15$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d16-x01-y01
Title=$B$ distribution for $<Q> = 18$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d17-x01-y01
Title=$B$ distribution for $<Q> = 24$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d18-x01-y01
Title=$B$ distribution for $<Q> = 37$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d19-x01-y01
Title=$B$ distribution for $<Q> = 58$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d20-x01-y01
Title=$B$ distribution for $<Q> = 81$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d21-x01-y01
Title=$B$ distribution for $<Q> = 116$GeV
XLabel=$B$
YLabel=$1/\sigma$ d$\sigma/$d$B$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d22-x01-y01
Title=$\rho_0$ distribution for $<Q> = 15$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d23-x01-y01
Title=$\rho_0$ distribution for $<Q> = 18$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d24-x01-y01
Title=$\rho_0$ distribution for $<Q> = 24$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d25-x01-y01
Title=$\rho_0$ distribution for $<Q> = 37$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d26-x01-y01
Title=$\rho_0$ distribution for $<Q> = 58$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d27-x01-y01
Title=$\rho_0$ distribution for $<Q> = 81$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d28-x01-y01
Title=$\rho_0$ distribution for $<Q> = 116$GeV
XLabel=$\rho_0$
YLabel=$1/\sigma$ d$\sigma/$d$\rho_0$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d29-x01-y01
Title=$C$-parameter distribution for $<Q> = 15$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d30-x01-y01
Title=$C$-parameter distribution for $<Q> = 18$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d31-x01-y01
Title=$C$-parameter distribution for $<Q> = 24$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d32-x01-y01
Title=$C$-parameter distribution for $<Q> = 37$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d33-x01-y01
Title=$C$-parameter distribution for $<Q> = 58$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d34-x01-y01
Title=$C$-parameter distribution for $<Q> = 81$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d35-x01-y01
Title=$C$-parameter distribution for $<Q> = 116$GeV
XLabel=$C$
YLabel=$1/\sigma$ d$\sigma/$d$C$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d36-x01-y01
Title=$<\tau_c>(Q)$ corrected to the hadron level
XLabel=$Q/$GeV
YLabel=$<\tau_c>$
LogY=0
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d36-x01-y02
Title=$<\tau>(Q)$ corrected to the hadron level
XLabel=$Q/$GeV
YLabel=$<\tau>$
LogY=0
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d36-x01-y03
Title=$<B>(Q)$ corrected to the hadron level
XLabel=$Q/$GeV
YLabel=$<B>$
LogY=0
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d36-x01-y04
Title=$<\rho_0>(Q)$ corrected to the hadron level
XLabel=$Q/$GeV
YLabel=$<\rho_0>$
LogY=0
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_2006_I699835/d36-x01-y05
Title=$<C>(Q)$ corrected to the hadron level
XLabel=$Q/$GeV
YLabel=$<C>$
LogY=0
# + any additional plot settings you might like, see make-plots documentation
END PLOT

# ... add more histograms as you need them ...
