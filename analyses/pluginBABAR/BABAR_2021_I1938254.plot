BEGIN PLOT /BABAR_2021_I1938254/d01-x01-y01
Title=Cross Section for $e^+e^-\to\pi^+\pi^-4\pi^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1938254/d02-x01-y01
Title=Cross Section for $e^+e^-\to\pi^+\pi^-\pi^0\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1938254/d03-x01-y01
Title=Cross Section for $e^+e^-\to\omega\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1938254/d04-x01-y01
Title=Cross Section for $e^+e^-\to\omega3\pi^0$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2021_I1938254/d05-x01-y01
Title=Cross Section for $e^+e^-\to\pi^+\pi^-3\pi^0\eta$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
