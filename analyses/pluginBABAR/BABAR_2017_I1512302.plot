BEGIN PLOT /BABAR_2017_I1512302/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/d01-x01-y02
Title=$\pi^\pm\pi^0$ mass distribution in $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m^2_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^\pm\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/dalitz_3pi
Title=Dalitz plot for $J/\psi\to \pi^+\pi^-\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^0}/{\rm d}m^2_{\pi^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2017_I1512302/d02-x01-y01
Title=$K^+K^-$ mass distribution in $J/\psi\to K^+K^-\pi^0$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/d02-x01-y02
Title=$K^\pm\pi^0$ mass distribution in $J/\psi\to K^+K^-\pi^0$
XLabel=$m^2_{K^\pm\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^\pm\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/dalitz_KpKmpi
Title=Dalitz plot for $J/\psi\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^0}/{\rm d}m^2_{K^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2017_I1512302/d03-x01-y01
Title=$K^0_SK^\pm$ mass distribution in $J/\psi\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^0_SK^\pm}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^\pm}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/d03-x01-y03
Title=$K^\pm\pi^\mp$ mass distribution in $J/\psi\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/d03-x01-y02
Title=$K^0_S\pi^\pm$ mass distribution in $J/\psi\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2017_I1512302/dalitz_dalitz_K0Kppim
Title=Dalitz plot for  $J/\psi\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^\pm\pi^\mp}/{\rm d}m^2_{K^0_S\pi^\pm}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
