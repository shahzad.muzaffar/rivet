BEGIN PLOT /BABAR_2011_I924163/d01-x01-y01
Title=$\Lambda_c^+\bar{\Lambda}^0$ mass distribution in $\bar{B}^0\to \Lambda_c^+\bar{\Lambda}^0 K^-$
XLabel=$m_{\Lambda_c^+\bar{\Lambda}^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\bar{\Lambda}^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I924163/d01-x01-y02
Title=$\bar{\Lambda}^0 K^-$ mass distribution in $\bar{B}^0\to \Lambda_c^+\bar{\Lambda}^0 K^-$
XLabel=$m_{\bar{\Lambda}^0 K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{\Lambda}^0 K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I924163/d01-x01-y03
Title=$\Lambda_c^+K^-$ mass distribution in $\bar{B}^0\to \Lambda_c^+\bar{\Lambda}^0 K^-$
XLabel=$m_{\Lambda_c^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
