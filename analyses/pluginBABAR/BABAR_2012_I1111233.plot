BEGIN PLOT /BABAR_2012_I1111233
LogY=0
XLabel=$q^2$ [$\text{GeV}^2$]
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d01-x0[1,2]-y01
Title=Differential branching ratio w.r.t $q^2$ for $B\to K\ell^+\ell^-$
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times 10^7$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d01-x0[1,2]-y02
Title=Differential branching ratio w.r.t $q^2$ for $B\to K^*\ell^+\ell^-$
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times 10^7$ [$\text{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d02-x0[1,2]-y01
Title=$A(CP)$ versus $q^2$ for $B\to K\ell^+\ell^-$
YLabel=$A(CP)$
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d02-x0[1,2]-y02
Title=$A(CP)$ versus $q^2$ for $B\to K^*\ell^+\ell^-$
YLabel=$A(CP)$
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d03-x0[1,2]-y01
Title=$R_K$ versus $q^2$ for $B\to K\ell^+\ell^-$
YLabel=$R_K$
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d03-x0[1,2]-y02
Title=$R_{K^*}$ versus $q^2$ for $B\to K^*\ell^+\ell^-$
YLabel=$R_{K*}$
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d04-x0[1,2]-y01
Title=$A_I$ versus $q^2$ for $B\to K\ell^+\ell^-$
YLabel=$A_I$
END PLOT
BEGIN PLOT /BABAR_2012_I1111233/d04-x0[1,2]-y02
Title=$A_I$ versus $q^2$ for $B\to K^*\ell^+\ell^-$
YLabel=$A_I$
END PLOT