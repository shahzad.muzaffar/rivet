BEGIN PLOT /BABAR_2015_I1403544/d03-x01-y03
Title=$K^0_SK^\pm$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^0_SK^\pm}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_SK^\pm}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/d03-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/d03-x01-y02
Title=$K^0_S\pi^\pm$ mass distribution in $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/dalitz_1
Title=Dalitz plot for  $\eta_c\to K^0_SK^\pm\pi^\mp$
XLabel=$m^2_{K^\pm\pi^\mp}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^\pm}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^\pm\pi^\mp}/{\rm d}m^2_{K^0_S\pi^\pm}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BABAR_2015_I1403544/d04-x01-y03
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/d04-x01-y01
Title=$K^+\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/d04-x01-y02
Title=$K^-\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2015_I1403544/dalitz_2
Title=Dalitz plot for  $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+\pi^0}/{\rm d}m^2_{K^-\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
