# BEGIN PLOT /ATLAS_2023_I2625697/d01-x01-y01
Title=TEEC for $p_\mathrm{T1}+p_\mathrm{T2} >$ 1000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi)$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d02-x01-y01
Title=TEEC for 1000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1200 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi)$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d03-x01-y01
Title=TEEC for 1200 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1400 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d04-x01-y01
Title=TEEC for 1400 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1600 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d05-x01-y01
Title=TEEC for 1600 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1800 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d06-x01-y01
Title=TEEC for 1800 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d07-x01-y01
Title=TEEC for 2000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2300 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d08-x01-y01
Title=TEEC for 2300 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2600 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d09-x01-y01
Title=TEEC for 2600 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 3000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d10-x01-y01
Title=TEEC for 3000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 3500 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d11-x01-y01
Title=TEEC for $p_\mathrm{T1}+p_\mathrm{T2} >$ 3500 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d12-x01-y01
Title=ATEEC for $p_\mathrm{T1}+p_\mathrm{T2} >$ 1000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d13-x01-y01
Title=ATEEC for 1000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1200 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d14-x01-y01
Title=ATEEC for 1200 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1400 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d15-x01-y01
Title=ATEEC for 1400 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1600 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d16-x01-y01
Title=ATEEC for 1600 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 1800 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d17-x01-y01
Title=ATEEC for 1800 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d18-x01-y01
Title=ATEEC for 2000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2300 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma \mathrm{d}\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d19-x01-y01
Title=ATEEC for 2300 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 2600 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma d\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d20-x01-y01
Title=ATEEC for 2600 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 3000 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma d\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d21-x01-y01
Title=ATEEC for 3000 GeV $< p_\mathrm{T1}+p_\mathrm{T2} <$ 3500 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma d\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2625697/d22-x01-y01
Title=ATEEC for $p_\mathrm{T1}+p_\mathrm{T2} >$ 3500 GeV
XLabel=$\cos\phi$
YLabel=$1/\sigma d\sigma^\mathrm{asym}/\mathrm{d}\cos\phi$
# END PLOT

