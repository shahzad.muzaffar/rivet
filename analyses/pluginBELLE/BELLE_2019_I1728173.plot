BEGIN PLOT /BELLE_2019_I1728173
LogY=0
XLabel=$q^2$ [$\text{GeV}^2$]
END PLOT

BEGIN PLOT /BELLE_2019_I1728173/d01-x0[1,2,3]-y03
Title=Ratio of $R_{K^*}$ for $B^+$ decays
YLabel=$R_{K^{*+}}$
END PLOT
BEGIN PLOT /BELLE_2019_I1728173/d01-x0[1,2,3]-y02
Title=Ratio of $R_{K^*}$ for $B^0$ decays
YLabel=$R_{K^{*0}}$
END PLOT
BEGIN PLOT /BELLE_2019_I1728173/d01-x0[1,2,3]-y01
Title=Ratio of $R_{K^*}$ for sum of $B^0$ and $B^+$ decays
YLabel=$R_{K*}$
END PLOT
