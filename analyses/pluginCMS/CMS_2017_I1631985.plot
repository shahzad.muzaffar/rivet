BEGIN PLOT /CMS_2017_I1631985/*
XMin=0.001
LogX=1
LogY=1
RatioPlotYMin=0.7
RatioPlotYMax=1.3
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d01-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$
XLabel=$\phi^{*}_{\eta}$
YLabel=$d\sigma/d\phi^{*}_{\eta}$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d02-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d\sigma/d\phi^{*}_{\eta}$
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d03-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$
ZMin=0.01
ZMax=10000
LogY=0
LogZ=1
XLabel=$\phi^{*}_{\eta}$
YLabel=$|y^{Z}|$
ZLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d04-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$
ZMin=0.00001
ZMax=10
LogY=0
LogZ=1
XLabel=$\phi^{*}_{\eta}$
YLabel=$|y^{Z}|$
ZLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0 < |y^{Z}| < 0.4$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y02
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0.4 < |y^{Z}| < 0.8$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y03
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0.8 < |y^{Z}| < 1.2$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y04
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($1.2 < |y^{Z}| < 1.6$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y05
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($1.6 < |y^{Z}| < 2.0$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d09-x01-y06
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($2.0 < |y^{Z}| < 2.4$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y01
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0 < |y^{Z}| < 0.4$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y02
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0.4 < |y^{Z}| < 0.8$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y03
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($0.8 < |y^{Z}| < 1.2$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y04
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($1.2 < |y^{Z}| < 1.6$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y05
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($1.6 < |y^{Z}| < 2.0$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT

BEGIN PLOT /CMS_2017_I1631985/d10-x01-y06
Title=CMS, 8 TeV, $Z \to \ell^+ \ell^-$ ($2.0 < |y^{Z}| < 2.4$)
XLabel=$\phi^{*}_{\eta}$
YLabel=$1/\sigma d^{2}\sigma/d\phi^{*}_{\eta}d|y^{Z}|$ [pb]
END PLOT