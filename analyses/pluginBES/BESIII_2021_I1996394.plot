BEGIN PLOT /BESIII_2021_I1996394/d01-x01-y01
Title=$R=\sigma(e^+e^-\to \mathrm{hadrons})/\sigma(e^+e^-\to \mu^+\mu^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$R$
LogY=0
ConnectGaps=1
END PLOT
