Name: BESIII_2022_I2166759
Year: 2022
Summary: $J/\psi$ and $\psi(2S)$ decays to $\eta\Sigma^+\bar\Sigma^-$
Experiment: BESIII
Collider: BEPC
InspireID: 2166759
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:210.09601 [hep-ex]
RunInfo:  Any process producing J/psi or psi(2S), originally e+e- 
Description:
'Measurements of mass distributions in $J/\psi$ and $\psi(2S)$ decays to $\eta\Sigma^+\bar\Sigma^-$.
The data were read from the plots in the paper and may not be corrected for efficiency but the
background given in the paper has been subtracted.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022ahw
BibTeX: '@article{BESIII:2022ahw,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of the $J/\psi$ and $\psi(3686)$ decays into $\eta\Sigma^{+}\overline{\Sigma}^{-}$}",
    eprint = "2210.09601",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "10",
    year = "2022"
}
'
