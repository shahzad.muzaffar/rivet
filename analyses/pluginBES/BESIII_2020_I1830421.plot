BEGIN PLOT /BESIII_2020_I1830421/d01-x01-y01
Title=Differential $\eta^\prime\to \pi^+\pi^-e^+e^-$ decay
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{e^+e^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2020_I1830421/d01-x01-y02
Title=Differential $\eta^\prime\to \pi^+\pi^-e^+e^-$ decay
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT