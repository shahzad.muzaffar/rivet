Name: BESIII_2022_I1997940
Year: 2022
Summary: EM Dalitz $J/\psi\to e^+e^- \eta^\prime\pi^+\pi^-$ decay
Experiment: BESIII
Collider: BEPC
InspireID: 1997940
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 129 (2022) 2, 022002
RunInfo: Any process producing J/psi originally e+e-
Description:
  'Measurement of the hadronic mass distribution in the decay $J/\psi\to e^+e^- \eta^\prime\pi^+\pi^-$. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2021xoh
BibTeX: '@article{BESIII:2021xoh,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of $J/\psi$ Electromagnetic Dalitz Decays to $X(1835)$, $X(2120)$ and $X(2370)$}",
    eprint = "2112.14369",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.129.022002",
    journal = "Phys. Rev. Lett.",
    volume = "129",
    number = "2",
    pages = "022002",
    year = "2022"
}
'
