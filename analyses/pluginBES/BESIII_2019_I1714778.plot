BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y01
Title=$K^0_S\pi^-$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{K^0_S\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y02
Title=$K^0_S\pi^+_1$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{K^0_S\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y03
Title=$K^0_S\pi^+_2$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{K^0_S\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y04
Title=$\pi^-\pi^+_1$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^-\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y05
Title=$\pi^-\pi^+_2$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^-\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y06
Title=$K^0_S\pi^-\pi^+_1$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{K^0_S\pi^-\pi^+_1}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-\pi^+_1}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y07
Title=$K^0_S\pi^-\pi^+_2$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{K^0_S\pi^-\pi^+_2}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-\pi^+_2}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y08
Title=$\pi^-\pi^+\pi^+$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^-\pi^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-\pi^+\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1714778/d01-x01-y09
Title=$\pi^+\pi^+$ mass distribution in $D^+\to K^0_S\pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
