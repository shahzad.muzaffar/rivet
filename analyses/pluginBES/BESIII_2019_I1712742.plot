BEGIN PLOT /BESIII_2019_I1712742/d01-x01-y01
Title=$D_s^+\to \eta e^+ \nu_e$ (using $\eta\to\gamma\gamma$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1712742/d01-x01-y02
Title=$D_s^+\to \eta e^+ \nu_e$ (using $\eta\to\gamma\pi^0\pi^+\pi^-$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BESIII_2019_I1712742/d02-x01-y01
Title=$D_s^+\to \eta^\prime e^+ \nu_e$ (using $\eta^\prime\to\gamma\gamma \pi^+\pi^-$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1712742/d02-x01-y02
Title=$D_s^+\to \eta^\prime e^+ \nu_e$ (using $\eta^\prime\to\gamma\rho$)
XLabel=$q^2$   [$\mathrm{GeV}^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$   [$\mathrm{ns}^{-1}\mathrm{GeV}^{-1}$]
END PLOT
